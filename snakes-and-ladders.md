# Introduction
Snakes and Ladders is an ancient Indian board game regarded today as a worldwide classic. 
It is played between two or more players on a gameboard having numbered, gridded squares. 
A number of "ladders" and "snakes" are pictured on the board, each connecting two specific board squares

# Tasks
Your task is to make a simple class called SnakesAndLadders.
which will support Two Players game with 2 dice. Each dice roll will be an
integer from 1 to 6. The player will move the sum of die1 and die2.

# Board
Design your board to look like the following: 
https://raw.githubusercontent.com/adrianeyre/codewars/master/Ruby/Authored/snakesandladdersboard.jpg

# Rules
1.  There are two players and both start off the board on square 0.

2.  Player 1 starts and alternates with player 2.

3.  You follow the numbers up the board in order 1=>100

4.  If the value of both die are the same then that player will have another go.

5.  Climb up ladders. The ladders on the game board allow you to move upwards and get ahead faster. 
If you land exactly on a square that shows an image of the bottom of a ladder, 
then you may move the player all the way up to the square at the top of the ladder. (even if you roll a double).

6.  Slide down snakes. Snakes move you back on the board because you have to slide down them.
If you land exactly at the top of a snake, slide move the player all the way to the square 
at the bottom of the snake or chute. (even if you roll a double).

7.  Land exactly on the last square to win. The first person to reach the highest square on the board wins.
[Optional] Let's add a twist! If you roll too high, your player "bounces" off the last square and moves back. 
You can only win by rolling the exact number needed to land on the last square. For example, if you are on 
square 98 and roll a five, move your game piece to 100 (two moves), then "bounce" back to 99, 98, 97 (three, four then five moves.)

# Returns
Return Player n Wins!. Where n is winning player that has landed on square 100 without any remainding moves left.

Return Game over! if a player has won and another player tries to play.

Otherwise return Player n is on square x. Where n is the current player and x is the sqaure they are currently on.

# Hints if stuck
1. Start by modeling the Board. How will you represent the Squares? How will you represent Snakes and Ladders. If you think about it,
a snake just means that if you land on the square you move a specific negative amount regardless of the dice roll. A ladder means that 
you move a specific positive amount regardless of the dice.

2. Now that you have your board ready, think about how to incorporate players. What new classes do you need? Do you have a class that keeps
track of the entire game? Both the players and the Board state. To keep things simple for now, you can explicitly set the dice rolls.

3. [Optional] Let's incorporate randomness in the dice rolls. Go for it!
